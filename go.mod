module foodly

go 1.15

require (
	github.com/facebook/ent v0.4.3
	github.com/gin-gonic/contrib v0.0.0-20201005132743-ca038bbf2944 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.4.1 // indirect
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/jackc/pgx/v4 v4.9.0
	github.com/mattn/go-sqlite3 v1.14.3
	github.com/nsf/gocode v0.0.0-20190302080247-5bee97b48836 // indirect
	github.com/ugorji/go v1.1.13 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/sys v0.0.0-20201024132449-ef9fd89ba245 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
